﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexEditor.Infrastructure
{
    public class HistoryByteContainer : ByteContainer
    {
        // Private types ------------------------------------------------------

        private class HistoryAction
        {
            public HistoryAction(ByteBufferChange change, int offset, int length, byte[] data, HistoryAction continueWith)
            {
                Change = change;
                Offset = offset;
                Length = length;
                Data = data;
                ContinueWith = continueWith;
            }

            public ByteBufferChange Change { get; }
            public int Offset { get; }
            public int Length { get; }
            public byte[] Data { get; }
            public HistoryAction ContinueWith { get; }
        }

        private class HistoryEntry
        {
            public HistoryEntry(HistoryAction undoAction, HistoryAction redoAction)
            {
                UndoAction = undoAction;
                RedoAction = redoAction;
            }

            public HistoryAction UndoAction { get; }
            public HistoryAction RedoAction { get; }
        }

        // Private fields -----------------------------------------------------

        private readonly List<HistoryEntry> history = new List<HistoryEntry>();

        // Private methods ----------------------------------------------------

        private void AddHistoryEntry(HistoryEntry entry)
        {
            // TODO take into account current position and clear history after
            // TODO limit history

            history.Add(entry);
        }

        // Public methods -----------------------------------------------------

        public HistoryByteContainer(int bucketSize) : base(bucketSize)
        {
        }

        public HistoryByteContainer(int bucketSize, BufferPool<byte> bufferPool) : base(bucketSize, bufferPool)
        {
        }

        public HistoryByteContainer(Stream stream, int bucketSize) : base(stream, bucketSize)
        {
        }

        public HistoryByteContainer(Stream stream, int bucketSize, BufferPool<byte> bufferPool) : base(stream, bucketSize, bufferPool)
        {
        }

        public override void Insert(int offset, byte target)
        {
            var undoAction = new HistoryAction(ByteBufferChange.Remove, offset, 1, null, null);

            base.Insert(offset, target);

            var redoAction = new HistoryAction(ByteBufferChange.Insert, offset, 1, new byte[] { target }, null);

            var entry = new HistoryEntry(undoAction, redoAction);
            AddHistoryEntry(entry);
        }

        public override void Insert(int offset, byte[] target, int targetOffset, int targetLength)
        {
            var undoAction = new HistoryAction(ByteBufferChange.Remove, offset, targetLength, null, null);

            base.Insert(offset, target, targetOffset, targetLength);

            byte[] data = new byte[targetLength];
            Array.Copy(target, targetOffset, data, 0, targetLength);

            var redoAction = new HistoryAction(ByteBufferChange.Insert, offset, targetLength, data, null);
            var entry = new HistoryEntry(undoAction, redoAction);
            AddHistoryEntry(entry);
        }

        public override void Remove(int offset, int length)
        {
            int availableBytes = CountAvailableBytes(offset, length);
            byte[] data = new byte[availableBytes];
            base.GetAvailableBytes(offset, availableBytes, data, 0);
            var undoAction = new HistoryAction(ByteBufferChange.Insert, offset, availableBytes, data, null);

            base.Remove(offset, length);

            var redoAction = new HistoryAction(ByteBufferChange.Remove, offset, length, null, null);
            var entry = new HistoryEntry(undoAction, redoAction);
            AddHistoryEntry(entry);
        }

        public override void Remove(int offset, RemoveMode mode)
        {
            int availableBytes = CountAvailableBytes(offset, 1);
            byte[] data = new byte[availableBytes];
            base.GetAvailableBytes(offset, availableBytes, data, 0);
            var undoAction = new HistoryAction(ByteBufferChange.Insert, offset, availableBytes, data, null);

            base.Remove(offset, mode);

            var redoAction = new HistoryAction(ByteBufferChange.Remove, offset, 1, null, null);
            var entry = new HistoryEntry(undoAction, redoAction);
            AddHistoryEntry(entry);
        }

        public override void Replace(int offset, byte target)
        {
            int availableBytes = CountAvailableBytes(offset, 1);
            byte[] data = new byte[availableBytes];
            base.GetAvailableBytes(offset, availableBytes, data, 0);
            var undoAction = new HistoryAction(ByteBufferChange.Replace, offset, 1, data, null);

            base.Replace(offset, target);

            var redoAction = new HistoryAction(ByteBufferChange.Replace, offset, 1, new[] { target }, null);
            var entry = new HistoryEntry(undoAction, redoAction);
            AddHistoryEntry(entry);
        }

        public override void Replace(int offset, byte[] target, int targetOffset, int length)
        {
            int availableBytes = CountAvailableBytes(offset, length);
            HistoryAction undoAction;
            byte[] data = new byte[availableBytes];
            Array.Copy(target, targetOffset, data, 0, availableBytes);
            if (availableBytes == length)
            {
                undoAction = new HistoryAction(ByteBufferChange.Replace, offset, length, data, null);
            }
            else
            {
                var insertAction = new HistoryAction(ByteBufferChange.Insert, offset, availableBytes, data, null);
                undoAction = new HistoryAction(ByteBufferChange.Remove, offset, length, null, insertAction);
            }

            base.Replace(offset, target, targetOffset, length);

            var redoAction = new HistoryAction(ByteBufferChange.Replace, offset, availableBytes, data, null);
            var entry = new HistoryEntry(undoAction, redoAction);
            AddHistoryEntry(entry);
        }
    }
}
