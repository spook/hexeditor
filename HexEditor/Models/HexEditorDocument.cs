﻿using HexEditor.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexEditor.Models
{
    public class HexEditorDocument : BaseViewModel
    {
        // Private constants --------------------------------------------------

        private const int BUCKET_SIZE = 10240; // 10 kb

        // Private fields -----------------------------------------------------

        private readonly ByteContainer byteContainer;
        private byte bytesPerRow = 16;

        // Private methods ----------------------------------------------------

        private void HandleContainerChanged(object sender, DataChangeEventArgs e)
        {
            DataChanged?.Invoke(this, e);
        }

        private void HandleBytesPerRowChanged()
        {
            SettingsChanged?.Invoke(this, EventArgs.Empty);
        }

        private void HandleContainerBeforeChange(object sender, DataChangeEventArgs args)
        {
            BeforeDataChanged?.Invoke(this, args);
        }

        private void HookContainerEvents()
        {
            byteContainer.Changed += HandleContainerChanged;
            byteContainer.BeforeChange += HandleContainerBeforeChange;
        }

        // Public methods -----------------------------------------------------

        public HexEditorDocument()
        {
            byteContainer = new ByteContainer(BUCKET_SIZE);
            HookContainerEvents();
        }

        public HexEditorDocument(Stream stream)
        {
            byteContainer = new ByteContainer(stream, BUCKET_SIZE);
            HookContainerEvents();
        }

        // Public properties --------------------------------------------------

        public ByteContainer ByteContainer => byteContainer;

        public byte BytesPerRow
        {
            get => bytesPerRow;
            set
            {
                if (value < 1)
                    throw new ArgumentOutOfRangeException(nameof(BytesPerRow));

                Set(ref bytesPerRow, () => BytesPerRow, value, HandleBytesPerRowChanged);
            }
        }

        public event DataChangeEventHandler BeforeDataChanged;

        public event DataChangeEventHandler DataChanged;

        public event EventHandler SettingsChanged;
    }
}
