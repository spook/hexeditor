﻿using HexEditor.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexEditorTester
{
    public class MainWindowViewModel
    {
        private HexEditorDocument document;

        public MainWindowViewModel()
        {
            byte[] data = new byte[16*16];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)(i % 256);
            }

            MemoryStream stream = new MemoryStream();
            stream.Write(data, 0, data.Length);
            stream.Seek(0, SeekOrigin.Begin);

            document = new HexEditorDocument(stream);
            
            // document = new HexEditorDocument();
        }

        public HexEditorDocument Document => document;
    }
}
